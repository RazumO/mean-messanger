const express = require('express');
const router = express.Router();
const authCheck = require('./../middleware/authCheck');
const extractFile = require('../middleware/extractFile');
const postsController = require('./../controllers/posts');

router.get('/:id', postsController.getPost);
router.get('', postsController.getPosts);
router.post('', authCheck, extractFile, postsController.createPost);
router.put('/:id', authCheck, extractFile, postsController.updatePost);
router.delete('/:id', authCheck, postsController.deletePost);

module.exports = router;
