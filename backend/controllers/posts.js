const Post = require('./../models/post');

exports.getPost = (req, res, next) => {
  Post.findById(req.params.id).then(post => {
    if (post) {
      res.status(200).json(post);
    } else {
      res.status(404).json({
        message: 'Post not found'
      });
    }
  }).catch(error => {
    res.status(500).json({message: 'DB error'})
  })
};

exports.getPosts = (req, res, next) => {
  const pageSize = Number(req.query.pagesize);
  const pageNumber = Number(req.query.page);
  const postsQuery = Post.find();
  let fetchedPosts;

  if (pageSize && pageNumber) {
    postsQuery.skip(pageSize * (pageNumber - 1)).limit(pageSize);
  }
  postsQuery.then(posts => {
    fetchedPosts = posts;
    return Post.count();
  }).then((postsCount) => {
    res.status(200).json({
      message: 'Posts fetched successfully',
      posts: fetchedPosts,
      totalCount: postsCount
    });
  }).catch(error => {
    res.status(500).json({message: 'DB error'})
  })
};

exports.createPost = (req, res, next) => {
  const fileName = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`;
  const post = new Post({
    title: req.body.title,
    content: req.body.content,
    imagePath: fileName,
    createdAt: (new Date()).toISOString(),
    creator: req.userData.userId
  });y

  post.save().then((createdPost) => {
    res.status(201).json({
      message: 'Post added successfully',
      post: {
        id: createdPost._id,
        title: createdPost.title,
        content: createdPost.content,
        imagePath: createdPost.imagePath
      }
    });
  }).catch(error => {
    res.status(500).json({message: 'DB error'})
  });
};

exports.updatePost = (req, res, next) => {
  let post;
  if (req.file) {
    const fileName = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`;
    post = new Post({
      _id: req.params.id,
      title: req.body.title,
      content: req.body.content,
      imagePath: fileName
    });
  } else {
    post = new Post({
      _id: req.params.id,
      title: req.body.title,
      content: req.body.content
    });
  }

  Post.updateOne({
    _id: req.params.id,
    creator: req.userData.userId
  }, post).then((result) => {
    console.log(result);
    if (result.n) {
      res.status(200).json({ message: 'Post Edited' });
    } else {
      res.status(401).json({message: 'User unauthorized'})
    }
  }).catch(error => {
    res.status(500).json({message: 'DB error'})
  });
};

exports.deletePost = (req, res, next) => {
  Post.deleteOne({
    _id: req.params.id,
    creator: req.userData.userId
  }).then((result) => {
    if (result.n) {
      res.status(200).json({ message: 'Post deleted' });
    } else {
      res.status(401).json({message: 'User unauthorized'})
    }
  }).catch(error => {
    res.status(500).json({message: 'DB error'})
  });
};
