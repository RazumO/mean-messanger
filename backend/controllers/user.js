const User = require('./../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('./../config.json');
const saltRounds = 10;

exports.userSignup = (req, res, next) => {
  // TODO Add password and email check !!
  const password = req.body.password;
  const email = req.body.email;

  bcrypt.hash(password, saltRounds)
    .then((hash) => {
        const user = new User({
          password: hash,
          email
        });
        return user.save();
    }).then((result) => {
      res.status(201).json({
        message: 'User succesfully created!',
        result
      })
    }).catch(error => {
      res.status(500).json({
        message: 'User authentication failed'
      })
    });
};

exports.userLogin = (req, res, next) => {
  const password = req.body.password;
  const email = req.body.email;
  let user;

  User.findOne({
    email
  }).then((userObject) => {
    if (!userObject) {
      return res.status(401).json({
        message: 'User authentication failed'
      });
    }
    user = userObject;
    return bcrypt.compare(password, user.password);
  }).then((result) => {
      if (!result) {
        return res.status(401).json({
          message: 'User authentication failed'
        });
      }
      const token = jwt.sign({
        email,
        userId: user._id
      }, config.secret, {
        expiresIn: '1h'
      });

      res.status(201).json({
        token,
        userId: user._id,
        expiresIn: 3600
      });
  }).catch(err => {
    res.status(401).json({
      message: 'User authentication failed'
    });
  });
};
