const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const app = express();
const postsRoutes = require('./routes/posts');
const userRoutes = require('./routes/user');
const config = require('./config.json');

mongoose.connect(config.connection_string)
  .then(() => {
    console.log('DB connected');
  })
  .catch(() => {
    console.log('DB failed to connect');
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/images', express.static(path.join('backend/images')));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requiested-With, Content-Type, Accept, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
  next();
});

app.use('/api/posts', postsRoutes)
app.use('/api/user', userRoutes)

module.exports = app;
