const multer = require('multer');

const MIME_TYPE_VALIDATOR = {
  'image/png': 'png',
  'image/jpg': 'jpg',
  'image/jpeg': 'jpg'
}

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const isValid = MIME_TYPE_VALIDATOR[file.mimetype];
    let error = null;
    if (!isValid) {
      error = new Error('Invalid image format. Acceptable are: ', MIME_TYPE_VALIDATOR.keys().join(', '));
    }
    cb(error, 'backend/images');
  },
  filename: (req, file, cb) => {
    const name = file.originalname.toLowerCase().replace(' ', '-') + '-' + Date.now();
    const ext = MIME_TYPE_VALIDATOR[file.mimetype];
    cb(null, `${name}.${ext}`);
  }
});

module.exports = multer({ storage }).single('image');


