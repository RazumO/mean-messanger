const jwt = require('jsonwebtoken');
const config = require('./../config.json');

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split('Bearer ')[1];
    const decodedToken = jwt.verify(token, config.secret);
    req.userData = {
      userId: decodedToken.userId,
      email: decodedToken.userId
    }
    if (decodedToken) {
      next();
    } else {
      throw('Auth failed');
    }
  } catch(e) {
    res.status(401).json({
      message: 'Auth failed'
    })
  }
}
