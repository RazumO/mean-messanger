1. install node 
2. install angular CLI by running -- npm install -g @angular/cli
3. install nodemon by running -- npm i -g nodemon
4. run server app by running -- npm run start:server
5. run front-end app by running npm start

Try server API http://localhost:3000/api/posts
Try front-end app http://localhost:4200/

