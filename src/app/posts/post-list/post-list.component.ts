import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from 'rxjs';

import { Post } from "../post.model";
import { PostsService } from "../posts.service";
import { PageEvent } from '@angular/material/paginator';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: "app-post-list",
  templateUrl: "./post-list.component.html",
  styleUrls: ["./post-list.component.css"]
})
export class PostListComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  isAuthenticated: boolean = false;
  posts: Post[] = [];
  totalPostsCount: number = 0;
  postsPerPage: number = 10;
  currentPage: number = 1;
  pageSizeOptions: number[] = [1, 2, 5, 10];
  userId: string;
  private postsSub: Subscription;
  private authSubscription: Subscription;

  constructor(public postsService: PostsService, private authService: AuthService) {}

  ngOnInit() {
    this.postsService.getPosts(this.postsPerPage, this.currentPage);
    this.isLoading = true;
    this.userId = this.authService.getUserId();
    this.postsSub = this.postsService.getPostUpdateListener()
      .subscribe((postsData: { posts: Post[], totalCount: number}) => {
        this.posts = postsData.posts;
        this.totalPostsCount = +postsData.totalCount;
        this.isLoading = false;
      });
    this.isAuthenticated = this.authService.getIsAuthenticated();
    this.authSubscription = this.authService.getUserStatusChanged().subscribe(isAuthinticated => {
      this.isAuthenticated = isAuthinticated;
      this.userId = this.authService.getUserId();
    });
  }

  ngOnDestroy() {
    this.postsSub.unsubscribe();
    this.authSubscription.unsubscribe();
  }

  onDelete(id: string) {
    this.postsService.deletePost(id).subscribe(() => {
      this.currentPage = 1;
      this.postsService.getPosts(this.postsPerPage, this.currentPage);
    });
  }

  onPageChange(pageInfo: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageInfo.pageIndex + 1;
    this.postsPerPage = pageInfo.pageSize;
    this.postsService.getPosts(this.postsPerPage, this.currentPage);
  }

}
