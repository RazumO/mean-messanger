import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

import { PostsService } from "../posts.service";
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Post } from '../post.model';

@Component({
  selector: "app-post-create",
  templateUrl: "./post-create.component.html",
  styleUrls: ["./post-create.component.css"]
})
export class PostCreateComponent implements OnInit {
  enteredTitle = "";
  enteredContent = "";
  isLoading: boolean = false;
  imagePreview: string = '';
  private mode: string = 'create';
  private postId: string;
  post: Post;
  form: FormGroup;

  constructor(public postsService: PostsService,
              public route: ActivatedRoute) {}

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [ Validators.required, Validators.minLength(3) ]
      }),
      content: new FormControl(null, {
        validators: [ Validators.required ]
      }),
      image: new FormControl(null, {
        validators: [ Validators.required ]
      })
    });
    this.route.paramMap.subscribe((routeParams: ParamMap) => {
      if (routeParams.has('postId')) {
        this.mode = 'edit';
        this.postId = routeParams.get('postId');
        this.isLoading = true;
        this.postsService.getPost(this.postId).subscribe(postData => {
          this.post = {
            id: postData._id,
            title: postData.title,
            content: postData.content,
            imagePath: postData.imagePath,
            creator: postData.creator
          };
          this.imagePreview = postData.imagePath;
          this.form.setValue({
            title: this.post.title,
            content: this.post.content,
            image: 'test'
          });
          this.isLoading = false;
        });
      } else {
        this.mode = 'create';
      }
    });
  }

  onAddPost() {
    if (this.form.invalid) {
      return;
    }
    if (this.mode === 'create') {
      this.postsService.addPost(this.form.value.title, this.form.value.content, this.form.value.image);
      this.form.reset();
    } else {
      this.postsService.editPost(this.postId, this.form.value.title, this.form.value.content, this.form.value.image);
    }
    this.isLoading = true;
  }

  onImagePick(eventTarget: HTMLInputElement) {
    const file = eventTarget.files[0];
    this.form.patchValue({
      image: file
    });
    this.form.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }
}
