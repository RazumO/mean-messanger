import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Post } from './post.model';
import { Router } from '@angular/router';

@Injectable({providedIn: 'root'})
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<{
    posts: Post[],
    totalCount: number
  }>();

  constructor(private http: HttpClient,
              private router: Router) {
  }

  getPosts(postsPerPage: number, pageNumber: number) {
    const queryParams = `?pagesize=${postsPerPage}&page=${pageNumber}`;
    return this.http.get<{ message: string, posts: Post[], totalCount: number }>('http://localhost:3000/api/posts' + queryParams).pipe(map(postsData => {
      return {
        posts: postsData.posts.map((post: any) => {
          return {
            title: post.title,
            content: post.content,
            id: post._id,
            imagePath: post.imagePath,
            creator: post.creator
          }
        }),
        totalCount: postsData.totalCount
      };
    })).subscribe((postsData) => {
      this.posts = postsData.posts;
      this.postsUpdated.next({
        posts: [...this.posts],
        totalCount: postsData.totalCount
      });
    });
  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  } the 

  getPost(id: string) {
    return this.http.get<{
      _id: string,
      title: string,
      content: string,
      imagePath: string;
      creator: string;
    }>(`http://localhost:3000/api/posts/${id}`);
  }

  addPost(title: string, content: string, image: File) {
    const formData = new FormData();
    formData.append('title', title);
    formData.append('content', content);
    formData.append('image', image);
    this.http.post<{ message: string, post: Post }>('http://localhost:3000/api/posts', formData).subscribe((responseData) => {
      this.router.navigate(['/']);
    });
  }

  // TODO Update post with image

  editPost(postId: string, title: string, content: string, image: File | string) {
    let post: Post | FormData;

    if (typeof image === 'object') {
      post = new FormData();
      post.append('title', title);
      post.append('content', content);
      post.append('image', image);
    } else {
      post = {title: title, content: content, imagePath: image, creator: ''};
    }
    this.http.put<{ message: string, postId: string }>(`http://localhost:3000/api/posts/${postId}`, post).subscribe((responseData) => {
      console.log(responseData.message);
      this.router.navigate(['/']);
    });
  }

  deletePost(id: string) {
    return this.http.delete(`http://localhost:3000/api/posts/${id}`);
  }
}
