import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [
    './header.component.css'
  ]
})
export class HeaderComponent implements OnInit, OnDestroy {
  isAuthinticated: boolean = false;
  private authSubscription: Subscription;
  constructor(private authService: AuthService) {

  }

  ngOnInit() {
    this.isAuthinticated = this.authService.getIsAuthenticated();
    this.authSubscription = this.authService.getUserStatusChanged().subscribe(isAuthinticated => {
      this.isAuthinticated = isAuthinticated;
    });
  }

  ngOnDestroy() {
    this.authSubscription.unsubscribe();
  }

  onLogout() {
    this.authService.logout();
  }

}
