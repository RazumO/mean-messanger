import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material.module';
import { AuthRouterModule } from './auth-router.module';
import { LoginComponent } from './LoginComponent/login.component';
import { SignupComponent } from './SignupComponent/signup.component';

@NgModule({
  declarations: [
    LoginComponent,
    SignupComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    AngularMaterialModule,
    AuthRouterModule
  ]
})
export class AuthModule {

}
