import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { AuthData } from './auth-data.model';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
}) export class AuthService {

  private token: string;
  private isAuthenticated: boolean = false;
  private expiresInTimeout: any;
  private userId: string;

  private $userStatusChanged = new Subject<boolean>();

  constructor(private http: HttpClient, private router: Router) {

  }

  getUserStatusChanged() {
    return this.$userStatusChanged.asObservable();
  }


  getToken() {
    return this.token;
  }

  getUserId() {
    return this.userId;
  }

  getIsAuthenticated(): boolean {
    return this.isAuthenticated;
  }

  createUser(email: string, password: string) {
    const authData: AuthData = {
      email,
      password
    };
    this.http.post('http://localhost:3000/api/user/signup', authData).subscribe(response => {
      console.log(response);
      this.router.navigate(['/login']);
    }, (error) => {
      console.warn(error);
      this.$userStatusChanged.next(false);
    });
  }

  login(email: string, password: string) {
    const authData: AuthData = {
      email,
      password
    };
    this.http.post<{token: string, expiresIn: number, userId: string  }>('http://localhost:3000/api/user/login', authData).subscribe(response => {
      this.token = response.token;
      this.userId = response.userId;
      if (this.token) {
        this.setExpiresInTimeout(response.expiresIn);
        this.isAuthenticated = true;
        this.saveAuthInfo(this.token, new Date(Date.now() + response.expiresIn * 1000), this.userId);
        this.$userStatusChanged.next(true);
        this.router.navigate(['/']);
      }
    }, (error) => {
      console.warn(error);
      this.$userStatusChanged.next(false);
    });
  }

  logout() {
    clearTimeout(this.expiresInTimeout);
    this.clearAuthInfo();
    this.isAuthenticated = false;
    this.token = null;
    this.userId = null;
    this.$userStatusChanged.next(this.isAuthenticated);
    this.router.navigate(['/']);
  }

  autoLogin(): void {
    const authInfo = this.getAuthInfo();
    if (!authInfo) {
      return;
    }
    const expiresIn = authInfo.expirationDate.getTime() - Date.now();
    if (expiresIn < 0) {
      return;
    }
    this.isAuthenticated = true;
    this.token = authInfo.token;
    this.userId = authInfo.userId;
    this.setExpiresInTimeout(expiresIn / 1000);
    this.$userStatusChanged.next(true);
  }

  private setExpiresInTimeout(expiresIn: number) {
    this.expiresInTimeout = setTimeout(() => {
      this.logout();
    }, expiresIn * 1000);
  }

  private getAuthInfo() {
    const token = localStorage.getItem('token');
    const expiration = localStorage.getItem('expiration');
    const userId = localStorage.getItem('userId');

    if (!token || !expiration || !userId) {
      return null;
    }
    return {
      token,
      expirationDate: new Date(expiration),
      userId
    }
  }


  private saveAuthInfo(token: string, expiresInDate: Date, userId: string) {
    localStorage.setItem('token', token);
    localStorage.setItem('userId', userId)
    localStorage.setItem('expiration', expiresInDate.toISOString());
  }

  private clearAuthInfo() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
    localStorage.removeItem('userId');
  }
}
