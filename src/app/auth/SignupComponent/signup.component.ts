import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from './../auth.service';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {
  isLoading: boolean = false;
  loginFailedSub: Subscription;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.loginFailedSub = this.authService.getUserStatusChanged().subscribe(() => {
      this.isLoading = false;
    });
  }

  ngOnDestroy() {
    this.loginFailedSub.unsubscribe();
  }


  onSignup(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    this.authService.createUser(form.value.email, form.value.password);
  }
}
